# Skeleton Program - AQA Examboard

## Data format for Saving
Order:
1. The # of symbols
2. The Symbols, one per line
3. The # of patterns
4. The Patterns, one per line, `[Symbol],[Pattern]`, `Pattern = [Symbol || "*"]`
5. Grid Size
6. Cells, one per line, loads grid from left to right, top to bottom `[Symbol],[Non-allowed Symbol](,)`
7. Turns left
