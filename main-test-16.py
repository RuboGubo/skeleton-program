#Skeleton Program code for the AQA A Level Paper 1 Summer 2024 examination
#this code should be used in conjunction with the Preliminary Material
#written by the AQA Programmer Team
#developed in the Python 3.9.4 programming environment

import random
import os # This will be a task that needs doing

def Main():
    Again = "y"
    Score = 0
    while Again == "y":
        Filename = input("Press Enter to start a standard puzzle or enter name of file to load: ")
        if len(Filename) > 0:
            MyPuzzle = Puzzle(Filename + ".txt")
        else:
            MyPuzzle = Puzzle(8, int(8 * 8 * 0.6))
        Score = MyPuzzle.AttemptPuzzle()
        print("Puzzle finished. Your score was: " + str(Score))
        Again = input("Do another puzzle? ").lower()

class Puzzle():
    def __init__(self, *args):
        if len(args) == 1: # load file
            self.__Score = 0
            self.__SymbolsLeft = 0
            self.__GridSize = 0
            self.__Grid = []
            self.__AllowedPatterns = []
            self.__AllowedSymbols = []
            self.__LoadPuzzle(args[0])
        else: # generate grid
            self.__Score = 0
            self.__SymbolsLeft = args[1] # The amount of symbols you are allowed to play
            self.__GridSize = args[0] # Grid is square, so only one dimension needed

            # Generate Grid
            self.__Grid = []
            for Count in range(1, self.__GridSize * self.__GridSize + 1):
                if random.randrange(1, 101) < 90:
                    C = Cell()
                else:
                    C = BlockedCell()
                self.__Grid.append(C)

            # # Set rules
            # Specifically the patterns and symbols that are allowed
            self.__AllowedPatterns = []
            self.__AllowedSymbols = []

            # Q Pattern
            QPattern = Pattern("Q", "QQ**Q**QQ")
            self.__AllowedPatterns.append(QPattern)
            self.__AllowedSymbols.append("Q")

            # X Pattern
            XPattern = Pattern("X", "X*X*X*X*X")
            self.__AllowedPatterns.append(XPattern)
            self.__AllowedSymbols.append("X")

            # T Pattern
            TPattern = Pattern("T", "TTT**T**T")
            self.__AllowedPatterns.append(TPattern)
            self.__AllowedSymbols.append("T")

    def __LoadPuzzle(self, Filename): # Will most likely have a save function as well.
        try:
            with open(Filename) as f:
                # See readme for documentation of load file.
                NoOfSymbols = int(f.readline().rstrip())
                for Count in range (1, NoOfSymbols + 1):
                    self.__AllowedSymbols.append(f.readline().rstrip())
                NoOfPatterns = int(f.readline().rstrip())
                for Count in range(1, NoOfPatterns + 1):
                    Items = f.readline().rstrip().split(",")
                    P = Pattern(Items[0], Items[1])
                    self.__AllowedPatterns.append(P)
                self.__GridSize = int(f.readline().rstrip())
                for Count in range (1, self.__GridSize * self.__GridSize + 1):
                    Items = f.readline().rstrip().split(",")
                    if Items[0] == "@":
                        C = BlockedCell()
                        self.__Grid.append(C)
                    else:
                        C = Cell()
                        C.ChangeSymbolInCell(Items[0])
                        for CurrentSymbol in range(1, len(Items)):
                            C.AddToNotAllowedSymbols(Items[CurrentSymbol])
                        self.__Grid.append(C)
                self.__Score = int(f.readline().rstrip())
                self.__SymbolsLeft = int(f.readline().rstrip())
        except:
            print("Puzzle not loaded")

    def AttemptPuzzle(self):
        Finished = False
        while not Finished:
            self.DisplayPuzzle()
            print("Current score: " + str(self.__Score))
            Row = -1
            Valid = False
            while not Valid:
                # Awful error checking
                # P-TODO: May ask me to fix this/ move to function
                # P-TODO: Add validation to make sure they are within the grid?
                try:
                    Row = int(input("Enter row number: "))
                    Valid = True
                except:
                    pass
            Column = -1
            Valid = False
            while not Valid:
                try:
                    Column = int(input("Enter column number: "))
                    Valid = True
                except:
                    pass
            Symbol = self.__GetSymbolFromUser()
            self.__SymbolsLeft -= 1
            CurrentCell: Cell = self.__GetCell(Row, Column)
            if CurrentCell.CheckSymbolAllowed(Symbol): # Does not check if a blocked cell was selected or not.
                if CurrentCell.GetSymbol() != "-":
                    self.RemoveSymbol(Symbol, Row, Column)
                CurrentCell.ChangeSymbolInCell(Symbol)
                AmountToAddToScore = self.CheckforMatchWithPattern(Row, Column)
                self.__Score += AmountToAddToScore
            if self.__SymbolsLeft == 0:
                Finished = True
        print()
        self.DisplayPuzzle()
        print()
        return self.__Score

    def __GetCell(self, Row, Column):
        Index = (self.__GridSize - Row) * self.__GridSize + Column - 1
        if Index >= 0:
            return self.__Grid[Index]
        else:
            raise IndexError()

    def CheckforMatchWithPattern(self, Row, Column, negative_scoring: bool = False):
        for StartRow in range(Row + 2, Row - 1, -1):
            for StartColumn in range(Column - 2, Column + 1):
                try:
                    # Spiral Pattern for some reason
                    PatternString = ""
                    PatternString += self.__GetCell(StartRow, StartColumn).GetSymbol()
                    PatternString += self.__GetCell(StartRow, StartColumn + 1).GetSymbol()
                    PatternString += self.__GetCell(StartRow, StartColumn + 2).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 1, StartColumn + 2).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 2, StartColumn + 2).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 2, StartColumn + 1).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 2, StartColumn).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 1, StartColumn).GetSymbol()
                    PatternString += self.__GetCell(StartRow - 1, StartColumn + 1).GetSymbol()
                    for P in self.__AllowedPatterns:
                        CurrentSymbol = self.__GetCell(Row, Column).GetSymbol()
                        if P.MatchesPattern(PatternString, CurrentSymbol):
                            # This makes sure that people cant place symbols in such a way, that people can overlap them.
                            # Note: This has a bug. It does not prevent people from extending the T pattern
                            #       up, meaning that it is possible to continue expanding it upwards cheaper than you should
                            #       while not having to pay for the stem element. e.g:
                            #
                            #       | | | |      |T|T|T|
                            #       |T|T|T|      |T|T|T|
                            #       | |T| |  ->  | |T| |
                            #       | |T| |      | |T| |
                            #
                            #       Only Three extra have to be added, for an extra pattern to be recognized.
                            #       In other words, this only blocks elements inside the 3x3 pattern, not around it.
                            if not negative_scoring:
                                self.__GetCell(StartRow, StartColumn).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow, StartColumn + 1).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow, StartColumn + 2).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn + 2).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn + 2).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn + 1).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn).AddToNotAllowedSymbols(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn + 1).AddToNotAllowedSymbols(CurrentSymbol)
                                return 10 # Points on success
                            else:
                                self.__GetCell(StartRow, StartColumn).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow, StartColumn + 1).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow, StartColumn + 2).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn + 2).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn + 2).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn + 1).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 2, StartColumn).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                self.__GetCell(StartRow - 1, StartColumn + 1).RemoveFromAllowedSymbolsList(CurrentSymbol)
                                return -10
                except:
                    pass
        return 0 # Points on fail
    
    def RemoveSymbol(self, new_symbol, row, column):
        Index = (self.__GridSize - row) * self.__GridSize + column - 1
        self.__Score += self.CheckforMatchWithPattern(row, column, True)
        new_cell = Cell()
        new_cell.ChangeSymbolInCell(new_symbol)

        self.__Grid[Index] = new_cell

    def __GetSymbolFromUser(self):
        Symbol = ""
        while not Symbol in self.__AllowedSymbols:
            Symbol = input("Enter symbol: ")
        return Symbol

    # This should not take self
    def __CreateHorizontalLine(self):
        Line = "  "
        for Count in range(1, self.__GridSize * 2 + 2):
            Line = Line + "-"
        return Line

    def DisplayPuzzle(self):
        print()
        if self.__GridSize < 10:
            print("  ", end='')
            # Creates labels for the top of the grid, but only when less than 10?
            for Count in range(1, self.__GridSize + 1):
                print(" " + str(Count), end='')
        print()
        print(self.__CreateHorizontalLine())
        # Now working on each row value (it is a 1d list that is then wrapped at the edges)
        for Count in range(0, len(self.__Grid)):
            # If at the beginning of a line
            if Count % self.__GridSize == 0 and self.__GridSize < 10:
                # Print line number
                print(str(self.__GridSize - ((Count + 1) // self.__GridSize)) + " ", end='')
            # Print grid sqare, e.i. `|X`
            print("|" + self.__Grid[Count].GetSymbol(), end='')
            # If at the end of the line
            if (Count + 1) % self.__GridSize == 0:
                # Ending marker, closes of 
                # `|X|X|X| |X` -> `|X|X|X| |X|`
                # This uses the default `end` attribute of the print function
                # which is a newline - THIS IS BAD CODE IT SHOULD NOT BE HIDDEN LIKE THIS!!!!!!!!!
                print("|")
                print(self.__CreateHorizontalLine())

class Pattern():
    def __init__(self, SymbolToUse, PatternString):
        self.__Symbol = SymbolToUse
        self.__PatternSequence = PatternString

    # This has many bugs
    def MatchesPattern(self, PatternString, SymbolPlaced):
        if SymbolPlaced != self.__Symbol:
            return False
        for Count in range(0, len(self.__PatternSequence)):
            try:
                if self.__PatternSequence[Count] == self.__Symbol and PatternString[Count] != self.__Symbol:
                    return False
            except Exception as ex:
                # Can't use ctrl+c to exit the program because of this.
                print(f"EXCEPTION in MatchesPattern: {ex}")
        return True

    # Unused!
    def GetPatternSequence(self):
      return self.__PatternSequence

class Cell():
    def __init__(self):
        self._Symbol = ""
        # This implements the not allowed to place logic.
        # it's fine
        self.__SymbolsNotAllowed = []

    def GetSymbol(self):
        if self.IsEmpty():
          return "-"
        else:
          return self._Symbol
    
    def IsEmpty(self):
        if len(self._Symbol) == 0:
            return True
        else:
            return False

    # should prob incorporate `CheckSymbolAllowed`, to prevent other
    # classes having to do work on this classes behalf, and return a result type.
    def ChangeSymbolInCell(self, NewSymbol):
        self._Symbol = NewSymbol

    

    def CheckSymbolAllowed(self, SymbolToCheck):
        # You can do this much better
        # ```return not SymbolToCheck in self.__SymbolsNotAllowed```
        for Item in self.__SymbolsNotAllowed:
            if Item == SymbolToCheck:
                return False
        return True

    def AddToNotAllowedSymbols(self, SymbolToAdd):
        self.__SymbolsNotAllowed.append(SymbolToAdd)

    def RemoveFromAllowedSymbolsList(self, symbol_to_remove):
        self.__SymbolsNotAllowed.remove(symbol_to_remove)

    # Obviously something for which there will be a question.
    def UpdateCell(self):
        pass

class BlockedCell(Cell):
    def __init__(self):
        # Hate inheritance
        super(BlockedCell, self).__init__()
        self._Symbol = "@"

    def CheckSymbolAllowed(self, SymbolToCheck):
        return False

if __name__ == "__main__":
    Main()