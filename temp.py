def sum_two_smallest_numbers(numbers):
    print(numbers)
    small = [10000000000000, 10000000000000]

    for number in numbers:
        if number < small[0]:
            small.pop(0)
            small.append(number)

        print(small)

    sum = 0

    for i in small:
        sum += i

    print(sum)
    return sum


print(sum_two_smallest_numbers([5, 8, 12, 18, 22]) == 13)
print(sum_two_smallest_numbers([7, 15, 12, 18, 22]) == 19)
print(sum_two_smallest_numbers([25, 42, 12, 18, 22]) == 30)