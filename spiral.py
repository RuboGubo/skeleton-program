from itertools import chain, repeat

def ncycles(iterable, n):
    "Returns the sequence elements n times."
    return chain.from_iterable(repeat(tuple(iterable), n))

# Create a spiral generator that can be rotated

# offset is always top left of spiral
def spiral(offset: list[int], size: int, rotation: str):
    directions = {
        "up": [[1, 0], [0, -1], [-1, 0], [0, 1]],
        "right": [[0, -1], [-1, 0], [0, 1], [1, 0]],
        "down": [[-1, 0], [0, 1], [1, 0], [0, -1]],
        "left": [[0, 1], [1, 0], [0, -1], [-1, 0]],
    }
    dirs = directions[rotation]

    # offset offsets so that they create the spiral in the right pace
    offsets: dict[str, list[int]] = {
        "up": [0, 0],
        "right": [size, 0],
        "down": [size, -size],
        "left": [0, -size],
    }
    print(offset)
    for index, dim_offset in zip(range(len(offset)), offsets[rotation]):
        offset[index] += dim_offset
    print(offset)

    current_size = size
    cycle = True
    for x, y in ncycles(dirs, size*2):
        for _ in range(current_size):
            offset = [offset[0] + x, offset[1] + y]
            print(f"({offset[0]}, {offset[1]})")
        
        if cycle:
            current_size -= 1
            cycle = False
        else:
            cycle = True

spiral([3, 3], 3, "down")