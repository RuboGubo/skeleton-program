# Question 11- Validation of Row and Column entries [5 marks] [Done]

Validate row and column number entries to only allow numbers within the grid size. 

## EAD
```py
    # inclusive
    def get_int(self, msg, min, max):
        if msg != "":
            print(msg)
        number = None
        while not ( type(number) == int and min <= int(number) <= max ):
            number = input(": ")
            try: number = int(number)
            except: ...
        return number

    def AttemptPuzzle(self):
        Finished = False
        while not Finished:
            self.DisplayPuzzle()
            print("Current score: " + str(self.__Score))

            Row = self.get_int("Enter Row number", 0, self.__GridSize)
            Column = self.get_int("Enter Column number", 0, self.__GridSize)

            Symbol = self.__GetSymbolFromUser()
            self.__SymbolsLeft -= 1
            CurrentCell = self.__GetCell(Row, Column)
            if CurrentCell.CheckSymbolAllowed(Symbol):
                CurrentCell.ChangeSymbolInCell(Symbol)
                AmountToAddToScore = self.CheckforMatchWithPattern(Row, Column)
                if AmountToAddToScore > 0:
                    self.__Score += AmountToAddToScore
            if self.__SymbolsLeft == 0:
                Finished = True
        print()
        self.DisplayPuzzle()
        print()
        return self.__Score
```
## Output
```sh
Press Enter to start a standard puzzle or enter name of file to load: 

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|-|-|-|-|-|-|-|
  -----------------
6 |-|-|-|@|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|@|-|
  -----------------
2 |-|-|-|-|@|-|-|-|
  -----------------
1 |-|-|@|-|-|-|-|-|
  -----------------
Current score: 0
Enter Row number
: 1
Enter Column number
: 100
: gfhsd
: 8
Enter symbol: Q

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|-|-|-|-|-|-|-|
  -----------------
6 |-|-|-|@|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|@|-|
  -----------------
2 |-|-|-|-|@|-|-|-|
  -----------------
1 |-|-|@|-|-|-|-|Q|
  -----------------
Current score: 0
Enter Row number
: 
```