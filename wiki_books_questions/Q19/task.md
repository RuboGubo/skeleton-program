# Question 19 - Advanced Wild Card [13 marks]

This questions refers to the class Puzzle. A new option of a wildcard is to be added to the game. When the player uses this option, they are given the opportunity to complete a pattern by overriding existing symbols to make that pattern. The wildcard can only be used once in a game.

Task 1:

Add a new option for the user that will appear before each turn. The user should be asked "Do you want to use your wildcard (Y/N)" If the user responds with "Y" then the Row, Column and Symbol will be taken as normal but then the new method ApplyWildcard should be called and the prompt for using the wildcard should no longer be shown in subsequent turns. If the user responds with "N" then the puzzle continues as normal.

Task 2:

Create a new method called ApplyWildcard that will take the Row, Column and Symbol as parameters and do the following:

1. Determine if the pattern can be completed in a 3x3 given the Row, Column and Symbol passed to it.

a) If the pattern can be made the cells in the pattern should be updated using the method UpdateCell() in the Cell class and 5 points added for move.

b) If the pattern cannot be made the user should be given the message "Sorry, the wildcard does not work for this cell – you have no wildcards left"

## EAD
```py

import random
import os
import copy

def __init__(self, *args):
    if len(args) == 1:
        self.__Score = 0
        self.__SymbolsLeft = 0
        self.__GridSize = 0
        self.__Grid = []
        self.__AllowedPatterns = []
        self.__AllowedSymbols = []
        self.__wildcard_used = False # -
        self.__LoadPuzzle(args[0])
    else:
        self.__Score = 0
        self.__SymbolsLeft = args[1]
        self.__GridSize = args[0]
        self.__Grid = []
        self.__wildcard_used = False # -
        for Count in range(1, self.__GridSize * self.__GridSize + 1):
            if random.randrange(1, 101) < 90:
                C = Cell()
            else:
                C = BlockedCell()
            self.__Grid.append(C)
        self.__AllowedPatterns = []
        self.__AllowedSymbols = []
        QPattern = Pattern("Q", "QQ**Q**QQ")
        self.__AllowedPatterns.append(QPattern)
        self.__AllowedSymbols.append("Q")
        XPattern = Pattern("X", "X*X*X*X*X")
        self.__AllowedPatterns.append(XPattern)
        self.__AllowedSymbols.append("X")
        TPattern = Pattern("T", "TTT**T**T")
        self.__AllowedPatterns.append(TPattern)
        self.__AllowedSymbols.append("T")

        self.__AllowedSymbols.append("*") # -

def AttemptPuzzle(self): # -
    Finished = False
    while not Finished:
        using_wildcard = False 
        self.DisplayPuzzle()
        print("Current score: " + str(self.__Score))
        if not self.__wildcard_used:
            print("Do you want to use your wildcard? (y/N)")
            if input(": ").upper() == "Y":
                using_wildcard = True
                self.__wildcard_used = True
        Row = -1
        Valid = False
        while not Valid:
            try:
                Row = int(input("Enter row number: "))
                Valid = True
            except:
                pass
        Column = -1
        Valid = False
        while not Valid:
            try:
                Column = int(input("Enter column number: "))
                Valid = True
            except:
                pass
        Symbol = self.__GetSymbolFromUser()
        self.__SymbolsLeft -= 1
        CurrentCell = self.__GetCell(Row, Column)

        if using_wildcard: self.__apply_wildcard(Row, Column)
        else: 
            if CurrentCell.CheckSymbolAllowed(Symbol):
                CurrentCell.ChangeSymbolInCell(Symbol)
                AmountToAddToScore = self.CheckforMatchWithPattern(Row, Column)
                if AmountToAddToScore > 0:
                    self.__Score += AmountToAddToScore
        if self.__SymbolsLeft == 0:
            Finished = True
    print()
    self.DisplayPuzzle()
    print()
    return self.__Score

def __apply_wildcard(self, Row, Column): # -
    old_grid = copy.deepcopy(self.__Grid)

    CurrentCell = self.__GetCell(Row, Column)

    CurrentCell.ChangeSymbolInCell("*")

    potential_score = self.CheckforMatchWithPattern(Row, Column)

    if potential_score > 0:
        self.__Score += 5
    else:
        self.__Grid = old_grid
        print("Sorry, the wildcard does not work for this cell – you have no wildcards left")

def MatchesPattern(self, PatternString, SymbolPlaced): # -
    if SymbolPlaced != self.__Symbol and SymbolPlaced != "*": 
        return False
    for Count in range(0, len(self.__PatternSequence)):
        try:
            if self.__PatternSequence[Count] == self.__Symbol and PatternString[Count] != self.__Symbol and PatternString[Count] != "*": # -
                return False
        except Exception as ex:
            print(f"EXCEPTION in MatchesPattern: {ex}")
    return True

```

## Output
```sh
Press Enter to start a standard puzzle or enter name of file to load: puzzle1

   1 2 3 4 5
  -----------
5 |Q|Q|@|-|-|
  -----------
4 |Q|Q|-|-|-|
  -----------
3 |-|X|Q|X|-|
  -----------
2 |-|-|X|-|-|
  -----------
1 |-|X|-|-|-|
  -----------
Current score: 10
Do you want to use your wildcard? (y/N)
: y
Enter row number: 5
Enter column number: 5
Enter symbol: Q
Sorry, the wildcard does not work for this cell – you have no wildcards left

   1 2 3 4 5
  -----------
5 |Q|Q|@|-|-|
  -----------
4 |Q|Q|-|-|-|
  -----------
3 |-|X|Q|X|-|
  -----------
2 |-|-|X|-|-|
  -----------
1 |-|X|-|-|-|
  -----------
Current score: 10
Enter row number: %                                                             
 skeleton-program  
 skeleton-program   cd /home/rubogubo/Documents/Projects/skeleton-program ; /usr/bin/env /home/rubogubo/.pyenv/versions/3.11.6/bin/python /home/rubogubo/.vscode-oss/extensions/ms-python.debugpy-2024.6.0-linux-x64/bundled/libs/debugpy/adapter/../../debugpy/launcher 43945 -- /home/rubogubo/Documents/Projects/skeleton-program/wiki_books_questions/Q19/main19.py
Press Enter to start a standard puzzle or enter name of file to load: puzzle1

   1 2 3 4 5
  -----------
5 |Q|Q|@|-|-|
  -----------
4 |Q|Q|-|-|-|
  -----------
3 |-|X|Q|X|-|
  -----------
2 |-|-|X|-|-|
  -----------
1 |-|X|-|-|-|
  -----------
Current score: 10
Do you want to use your wildcard? (y/N)
: n
Enter row number: 1
Enter column number: 4
Enter symbol: X

   1 2 3 4 5
  -----------
5 |Q|Q|@|-|-|
  -----------
4 |Q|Q|-|-|-|
  -----------
3 |-|X|Q|X|-|
  -----------
2 |-|-|X|-|-|
  -----------
1 |-|X|-|X|-|
  -----------
Current score: 20
Do you want to use your wildcard? (y/N)
: y
Enter row number: 1
Enter column number: 4
Enter symbol: T


   1 2 3 4 5
  -----------
5 |Q|Q|@|-|-|
  -----------
4 |Q|Q|-|-|-|
  -----------
3 |-|X|Q|X|-|
  -----------
2 |-|-|X|-|-|
  -----------
1 |-|X|-|*|-|
  -----------

Puzzle finished. Your score was: 25
Do another puzzle?
```