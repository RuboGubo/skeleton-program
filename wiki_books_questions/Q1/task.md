# Question 1 - Symbol Case [3 marks] [Correct - 1min]

Lower case symbols are not accepted. E.g. if you enter 'q' it is not recognized as 'Q'. Fix this. 

## EAD
```py
def __GetSymbolFromUser(self):
    Symbol = ""
    while not Symbol in self.__AllowedSymbols:
        Symbol = input("Enter symbol: ").upper()
    return Symbol
```