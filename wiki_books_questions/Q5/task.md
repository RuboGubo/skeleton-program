# Question 5 - Save current game (status) [5 marks] [Done - overtime]

Save the current status of the game (file-handling)/writing to a text file. 

## EAD
```py
def Main():
    Again = "y"
    Score = 0
    while Again == "y":
        Filename = input("Press Enter to start a standard puzzle or enter name of file to load: ")
        if len(Filename) > 0:
            MyPuzzle = Puzzle(Filename + ".txt")
        else:
            MyPuzzle = Puzzle(8, int(8 * 8 * 0.6))
        MyPuzzle.SavePuzzle("temp")
        Score = MyPuzzle.AttemptPuzzle()
        print("Puzzle finished. Your score was: " + str(Score))
        Again = input("Do another puzzle? ").lower()

def SavePuzzle(self, filename: str):
    with open(filename, "w") as handle:
        serialized = ""
        serialized += str(len(self.__AllowedSymbols)) + "\n"
        for symbol in self.__AllowedSymbols:
            serialized += f"{symbol}\n"
        serialized += str(len(self.__AllowedPatterns))+"\n"
        for pattern in self.__AllowedPatterns:
            string_pattern = pattern.GetPatternSequence()
            pattern_symbol = pattern.get_symbol()
            serialized +=  f"{pattern_symbol},{string_pattern}\n"
        serialized += str(self.__GridSize) + "\n"
        for cell in self.__Grid:
            cell_symbol = cell.GetSymbol()
            if cell_symbol == "-":
                cell_symbol = ""
            not_allowed = cell.get_not_allowed_symbols()
            temp_s = ""
            for symbol in not_allowed:
                temp_s += ","+symbol
            serialized += f"{cell_symbol}{temp_s}\n"
        serialized += str(self.__Score) + "\n"
        serialized += str(self.__SymbolsLeft)

        print(serialized)
        handle.write(serialized)

```

## Output
```sh

```