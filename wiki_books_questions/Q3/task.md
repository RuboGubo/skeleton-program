# Question 3 - Blow up a block (blocked cell) [10 marks] [Done - 10min]

Have a 'bomb' that can remove or 'blow-up' a block in a 'blocked cell', but costs you some of your score (minus some points):  

## EAD
218
60
139
264
```py
    def AttemptPuzzle(self):
        Finished = False
        while not Finished:
            self.DisplayPuzzle()
            print("Current score: " + str(self.__Score))
            Row = -1
            Valid = False
            while not Valid:
                try:
                    Row = int(input("Enter row number: "))
                    Valid = True
                except:
                    pass
            Column = -1
            Valid = False
            while not Valid:
                try:
                    Column = int(input("Enter column number: "))
                    Valid = True
                except:
                    pass
            Symbol = self.__GetSymbolFromUser()
            self.__SymbolsLeft -= 1
            CurrentCell = self.__GetCell(Row, Column)
            if CurrentCell.CheckSymbolAllowed(Symbol):
                if CurrentCell._Symbol == "@" and Symbol == "B":
                    self.__SetCell(Row, Column, Cell())
                    self.__Score -= 5
                else:
                    CurrentCell.ChangeSymbolInCell(Symbol)
                    AmountToAddToScore = self.CheckforMatchWithPattern(Row, Column)
                    if AmountToAddToScore > 0:
                        self.__Score += AmountToAddToScore
            if self.__SymbolsLeft == 0:
                Finished = True
        print()
        self.DisplayPuzzle()
        print()
        return self.__Score
```
```py
class Puzzle():
    def __init__(self, *args):
        if len(args) == 1:
            self.__Score = 0
            self.__SymbolsLeft = 0
            self.__GridSize = 0
            self.__Grid = []
            self.__AllowedPatterns = []
            self.__AllowedSymbols = []
            if not self.__LoadPuzzle(args[0]):
                self.__GeneratePuzzle([8, int(8 * 8 * 0.6)])
        else:
            self.__GeneratePuzzle(args)

    def __GeneratePuzzle(self, args):
        self.__Score = 0
        self.__SymbolsLeft = args[1]
        self.__GridSize = args[0]
        self.__Grid = []
        for Count in range(1, self.__GridSize * self.__GridSize + 1):
            if random.randrange(1, 101) < 90:
                C = Cell()
            else:
                C = BlockedCell()
            self.__Grid.append(C)
        self.__AllowedPatterns = []
        self.__AllowedSymbols = []
        QPattern = Pattern("Q", "QQ**Q**QQ")
        self.__AllowedPatterns.append(QPattern)
        self.__AllowedSymbols.append("Q")
        XPattern = Pattern("X", "X*X*X*X*X")
        self.__AllowedPatterns.append(XPattern)
        self.__AllowedSymbols.append("X")
        TPattern = Pattern("T", "TTT**T**T")
        self.__AllowedPatterns.append(TPattern)
        self.__AllowedSymbols.append("T")

        # Bom symbol
        self.__AllowedSymbols.append("B")

```
```py
class Cell():
    def __init__(self):
        self._Symbol = ""
        self.__SymbolsNotAllowed = ["B"]
```

```py
class BlockedCell(Cell):
    def __init__(self):
        super(BlockedCell, self).__init__()
        self._Symbol = "@"

    def CheckSymbolAllowed(self, SymbolToCheck):
        return SymbolToCheck == "B"
```

```py
    def __SetCell(self, Row, Column, cell):
        Index = (self.__GridSize - Row) * self.__GridSize + Column - 1
        if Index >= 0:
            self.__Grid[Index] = cell
        else:
            raise IndexError()
```


## Output
```sh
Press Enter to start a standard puzzle or enter name of file to load: 

   1 2 3 4 5 6 7 8
  -----------------
8 |@|-|-|-|-|-|-|-|
  -----------------
7 |-|-|-|-|@|-|-|-|
  -----------------
6 |-|-|-|-|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|@|@|
  -----------------
3 |-|-|-|-|-|-|-|@|
  -----------------
2 |-|-|-|-|-|-|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 8
Enter column number: 1
Enter symbol: B

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|-|-|-|@|-|-|-|
  -----------------
6 |-|-|-|-|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|@|@|
  -----------------
3 |-|-|-|-|-|-|-|@|
  -----------------
2 |-|-|-|-|-|-|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: -5
Enter row number:
```