# Question 2 - Game file not existing [4 marks] [Done]

If a filename is entered that does not exist, the game is unplayable (infinite loop). Amend the program so that in this case the default game is played, with a suitable message to indicate this. 

## EAD
```py
class Puzzle():
    def __init__(self, *args):
        if len(args) == 1:
            self.__Score = 0
            self.__SymbolsLeft = 0
            self.__GridSize = 0
            self.__Grid = []
            self.__AllowedPatterns = []
            self.__AllowedSymbols = []
            if not self.__LoadPuzzle(args[0]):
                self.__GeneratePuzzle(args)
        else:
            self.__GeneratePuzzle(args)

    def __GeneratePuzzle(self, args):
        self.__Score = 0
        self.__SymbolsLeft = args[1]
        self.__GridSize = args[0]
        self.__Grid = []
        for Count in range(1, self.__GridSize * self.__GridSize + 1):
            if random.randrange(1, 101) < 90:
                C = Cell()
            else:
                C = BlockedCell()
            self.__Grid.append(C)
        self.__AllowedPatterns = []
        self.__AllowedSymbols = []
        QPattern = Pattern("Q", "QQ**Q**QQ")
        self.__AllowedPatterns.append(QPattern)
        self.__AllowedSymbols.append("Q")
        XPattern = Pattern("X", "X*X*X*X*X")
        self.__AllowedPatterns.append(XPattern)
        self.__AllowedSymbols.append("X")
        TPattern = Pattern("T", "TTT**T**T")
        self.__AllowedPatterns.append(TPattern)
        self.__AllowedSymbols.append("T")

    def __LoadPuzzle(self, Filename):
        try:
            with open(Filename) as f:
                NoOfSymbols = int(f.readline().rstrip())
                for Count in range (1, NoOfSymbols + 1):
                    self.__AllowedSymbols.append(f.readline().rstrip())
                NoOfPatterns = int(f.readline().rstrip())
                for Count in range(1, NoOfPatterns + 1):
                    Items = f.readline().rstrip().split(",")
                    P = Pattern(Items[0], Items[1])
                    self.__AllowedPatterns.append(P)
                self.__GridSize = int(f.readline().rstrip())
                for Count in range (1, self.__GridSize * self.__GridSize + 1):
                    Items = f.readline().rstrip().split(",")
                    if Items[0] == "@":
                        C = BlockedCell()
                        self.__Grid.append(C)
                    else:
                        C = Cell()
                        C.ChangeSymbolInCell(Items[0])
                        for CurrentSymbol in range(1, len(Items)):
                            C.AddToNotAllowedSymbols(Items[CurrentSymbol])
                        self.__Grid.append(C)
                self.__Score = int(f.readline().rstrip())
                self.__SymbolsLeft = int(f.readline().rstrip())
            return True
        except:
            print("Puzzle not loaded")
            return False

```

## Output
```sh
Press Enter to start a standard puzzle or enter name of file to load: h
Puzzle not loaded

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|-|-|-|-|-|-|-|
  -----------------
6 |-|-|-|-|-|-|-|-|
  -----------------
5 |-|-|-|@|-|@|-|-|
  -----------------
4 |-|-|-|-|-|@|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |-|-|-|-|-|-|-|-|
  -----------------
1 |-|-|@|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number:
```