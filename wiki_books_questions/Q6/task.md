# Question 6 - Rotated letter/symbol [9 marks]

Score a 'rotated' symbol/letter (lower score?)

## EAD
```py
def gen_pattern_string(self, StartRow, StartColumn, rotation: str):
    PatternString = ""
    if rotation == "up":
        PatternString += self.__GetCell(StartRow, StartColumn).GetSymbol()
        PatternString += self.__GetCell(StartRow, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 1).GetSymbol()
    elif rotation == "right":
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 1).GetSymbol()
    elif rotation == "down":
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 1).GetSymbol()
    elif rotation == "left":
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 0).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 0, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 2).GetSymbol()
        PatternString += self.__GetCell(StartRow - 2, StartColumn + 1).GetSymbol()
        PatternString += self.__GetCell(StartRow - 1, StartColumn + 1).GetSymbol()
    return PatternString


def set_not_allowed(self, StartRow, StartColumn, Symbol):
    self.__GetCell(StartRow, StartColumn).AddToNotAllowedSymbols(Symbol) 
    self.__GetCell(StartRow, StartColumn + 1).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow, StartColumn + 2).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 1, StartColumn + 2).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 2, StartColumn + 2).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 2, StartColumn + 1).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 2, StartColumn).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 1, StartColumn).AddToNotAllowedSymbols(Symbol)
    self.__GetCell(StartRow - 1, StartColumn + 1).AddToNotAllowedSymbols(Symbol)

def CheckforMatchWithPattern(self, Row, Column):
    for StartRow in range(Row + 2, Row - 1, -1):
        for StartColumn in range(Column - 2, Column + 1):
            try:
                PatternString = self.gen_pattern_string(StartRow, StartColumn, "up")
                CurrentSymbol = self.__GetCell(Row, Column).GetSymbol()
                for pattern in self.__AllowedPatterns:
                    if pattern.MatchesPattern(PatternString, CurrentSymbol):
                        self.set_not_allowed(StartRow, StartColumn, CurrentSymbol)
                        return 10
                for rotation in ["right", "down", "left"]:
                    PatternString = self.gen_pattern_string(StartRow, StartColumn, rotation)
                    for pattern in self.__AllowedPatterns:
                        if pattern.MatchesPattern(PatternString, CurrentSymbol):
                            self.set_not_allowed(StartRow, StartColumn, CurrentSymbol)
                            return 5
            except Exception as e:
                print("error", e)
                pass
    return 0
```

## Output
```sh
Press Enter to start a standard puzzle or enter name of file to load: 

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|-|-|@|-|-|@|
  -----------------
6 |-|-|-|-|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 6
Enter column number: 1
Enter symbol: Q
error 
error 

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|-|-|@|-|-|@|
  -----------------
6 |Q|-|-|-|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 6
Enter column number: 2
Enter symbol: Q
error 

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|-|-|@|-|-|@|
  -----------------
6 |Q|Q|-|-|-|-|-|-|
  -----------------
5 |-|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 5
Enter column number: 1
Enter symbol: Q

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|-|-|@|-|-|@|
  -----------------
6 |Q|Q|-|-|-|-|-|-|
  -----------------
5 |Q|-|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 5
Enter column number: 2
Enter symbol: Q

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|-|-|@|-|-|@|
  -----------------
6 |Q|Q|-|-|-|-|-|-|
  -----------------
5 |Q|Q|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 0
Enter row number: 7
Enter column number: 3
Enter symbol: Q
error 
error 
error 

   1 2 3 4 5 6 7 8
  -----------------
8 |-|-|-|-|-|-|-|-|
  -----------------
7 |-|@|Q|-|@|-|-|@|
  -----------------
6 |Q|Q|-|-|-|-|-|-|
  -----------------
5 |Q|Q|-|-|-|-|-|-|
  -----------------
4 |-|-|-|-|-|-|-|-|
  -----------------
3 |-|-|-|-|-|-|-|-|
  -----------------
2 |@|-|-|-|-|@|-|-|
  -----------------
1 |-|-|-|-|-|-|-|-|
  -----------------
Current score: 5
Enter row number:
```